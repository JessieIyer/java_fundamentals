/* 1. USE OF GIT AND COMMITS IN ECLIPSE
 * 2. USE OF BASIC DATA TYPES
 * 3. DEBUGGING USING BREAK POINTS
 */

/*
 * STEPS TO FOLLOW:

>> Open perspective > GIT ; Switch perspective again by open perspective > Java
>> To get the project file location: Right click on project name > Show in > System Explorer > get path of project root folder
>> Switch back to GIT perspective > Create a new local repository > Browse > Paste copied path of project > Create
>> Go back to playground folder and check if .git is created.
>> Right click program folder > Git bash here > give git status
>> Back on Eclipse, Go to Git Staging 
>> Download gitignore eclipse template from google
>> Right click project> New file> filename: .gitignore> paste copied contents from downloaded link > save. ; Use of .gitignore in line 1 says the git to ignore the gitignore file (especially bin folder- containing compiled .class files) as a whole from committing 
>> Adding .classpath and .project to .gitignore file stops pushing the class files into local repo.
>> Switch back to GIT perspective > GIT Staging > number of unstaged changes will be less (only 3 here)
>> To move unstaged changes to staged changes -- In GIT staging click on ++ (git add .) ;  Open GIT Bash terminal > git status (The untracked files are now tracked)
>> On Commit message > enter commit message > Commit ; Onto History, we can see the tracked changes.
>> On GIT Bash > git status > no commits ; git log --oneline  > shows commit message.
>> SQUASHING commits -- Combining multiple commits into a single one; keeps GIT history organized.
>> To squash commits -- History > Right click on the commits to be squashed > Modify > Squash.
 */

package com.example;

public class Main {

	public static void main(String[] args) {
		final int cannotReassign = 20;
		System.out.println(cannotReassign);
		// cannotReassign = 35; // Error- final variable can't be over written.
		int aPrimitive = 28; // primitive data types - int, float, char --No default methods
		String nonPrimitive = "Jessie"; // Non primitive data type - Strings, classes -- Many default methods.

		int result = nonPrimitive.compareToIgnoreCase("jeSSie");
		System.out.println(result);

		char identifyChar = 78;
		System.out.println(identifyChar); // Prints ASCII character equivalent to 78

		byte aLittleByteBox;
		// int muchBiggerByteBox = 129;
		int muchBiggerByteBox = 120; // works! as within byte range.
		aLittleByteBox = (byte) muchBiggerByteBox;
		System.out.println(aLittleByteBox); // O/P = -127 cuz, byte range is only from -127 to +128

		int i = 10;
		System.out.println(i++);
		System.out.println(++i);

		String name = "Jessie";
		// name = null;

		/* LINE 53, 54 - DEAD CODE, CODE JUST COMPILED BUT NEVER EXECUTED */

		if (name != null) {
			System.out.println(name);
		}

		// DATA STRUCTURES
		int[] myArr = { 1, 2, 3, 4, 5 };
		int j = 0;
		System.out.println(myArr); // prints base index - myArr[0] address

		for (j = 0; j < myArr.length; j++) {
			System.out.println(myArr[j]);
		}
		System.out.println("\n");

		// USE OF FOR EACH LOOP - meant for read-only
		for (int k : myArr) {
			System.out.println(k);
		}

		// STATIC AND NON STATIC METHODS
		Main.staticMethod("Jayashree");

		Main aMainInstance = new Main();
		aMainInstance.nonStaticMethod("Arjun");

		// STRING OPERATIONS
		System.out.println("world the high".toUpperCase());
		System.out.println(name.isEmpty());
	}

	// Changes are to be committed every now and then.

	static void staticMethod(String name) {
		System.out.println("Hello " + name);
	}

	void nonStaticMethod(String name) {
		System.out.println("Hello " + name);
	}
}

/*
 * SHORTCUTS: open downloaded xml templates: _ '+' Ctrl '+' Space open
 * variables: _2 '+' Ctrl '+' Space print system.out.println : sysout '+' Ctrl
 * '+' Space type in variables : variable first two alphabets '+' Ctrl '+' Space
 */
